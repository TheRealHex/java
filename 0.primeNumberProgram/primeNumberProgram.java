import java.util.Scanner; //Imports scanner class

public class primeNumberProgram {
  public static void main(String[] args) {
    Scanner obj = new Scanner(System.in);
    System.out.println("Enter the number: ");

    int number = obj.nextInt();
    boolean flag = false;
    for(int i=2; i <= number / 2; ++i){
      if (number % i == 0){
        flag = true;
        break;
      }
    }

    if(!flag)
      System.out.println(number + " is a prime number."); 
    else
      System.out.println(number + " is not a prime number.");
  }
}
