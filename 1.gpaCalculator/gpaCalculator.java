import java.util.Scanner;

public class gpaCalculator {
  public static void main(String[] args) {
    Scanner obj = new Scanner(System.in);
    System.out.println("Enter the marks for 3 subjects.");

    float subj1 = obj.nextInt();
    float subj2 = obj.nextInt();
    float subj3 = obj.nextInt();
    float totalMarks = subj1 + subj2 + subj3;
    float gpa = (totalMarks/3) / 20 - 1;

    if(subj1 > 100 || subj1 < 0 || subj2 > 100 || subj2 < 0 || subj3 > 100 || subj3 < 0){
      System.out.println("Invalid entry.");
    }else{
      System.out.printf("Percentage: %.2f \n", totalMarks/3);
      System.out.printf("GPA: %.2f \n", gpa);
    }
  }
}
